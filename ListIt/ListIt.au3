;
; File List Version 3.1
;
; A Windows utility to create a text list of folder contents.
; Written in au3 scripting language for AutoIt V3.
; http://www.autoitscript.com
;
; Copyright (c) 2007, Peter Russell
;

#include <GUIConstants.au3>
#include <GuiListBox.au3>
#include <String.au3>
#include <Array.au3>

#Region ### START Koda GUI section ### Form=D:\Data\My AutoIT\ListIt\ListIt.kxf
$FormMain = GUICreate("ListIt", 330, 492, 194, 115)
$Path = GUICtrlCreateInput("", 4, 4, 285, 21)
$Dir = GUICtrlCreateButton("...", 296, 4, 25, 25, 0)
$List = GUICtrlCreateList("", 4, 32, 317, 396)
$Copy = GUICtrlCreateButton("Copy to Clipboard", 8, 440, 313, 25, 0)
$MenuFile = GUICtrlCreateMenu("&File")
$MenuOpen = GUICtrlCreateMenuItem("Open", $MenuFile)
$MenuPath = GUICtrlCreateMenuItem("Explorer Path", $MenuFile)
$MenuCopy = GUICtrlCreateMenuItem("Copy to Clipboard", $MenuFile)
$MenuClose = GUICtrlCreateMenuItem("Close", $MenuFile)
$MenuHelp = GUICtrlCreateMenu("&Help")
$MenuSummary = GUICtrlCreateMenuItem("Summary", $MenuHelp)
$MenuAbout = GUICtrlCreateMenuItem("About", $MenuHelp)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

; Help Text

$HT =       "AU3Tools - AutoIT Utility Software" & @CRLF & @CRLF
$HT = $HT & "ListIt (Version 3.0) is a tool that creates a file list as text and copies it to the clipboard." & @CRLF & @CRLF
$HT = $HT & "LICENSE" & @CRLF & @CRLF
$HT = $HT & "The ListIt (Version 3.0) tool is provided free for non-commercial personal use. This software is copyright "
$HT = $HT & "and any form of modification, decompilation, hacking or repackaging is strictly prohibited. You will always be able to "
$HT = $HT & "obtain this software free of charge within these provisions and you should never pay anyone for it. Please share "
$HT = $HT & "it with your friends. As far as practicable after testing I warrant that to the best of my knowledge this software "
$HT = $HT & "does what the documentation claims and does not contain any malicious elements or spyware. I cannot be held "
$HT = $HT & "responsible for hacked, infected or trojan versions that may appear later by the hand of others. I recommend "
$HT = $HT & "that you obtain a copy of this program from a reputable site or trusted friend." & @CRLF & @CRLF
$HT = $HT & "DISCLAIMER" & @CRLF & @CRLF
$HT = $HT & "Please use ListIt, but only at your own risk, as I cannot be held responsible for any subsequent damage or losses "
$HT = $HT & "that may occur for any reason associated with its use or misuse." & @CRLF & @CRLF
$HT = $HT & "COMMERCIAL USE" & @CRLF & @CRLF
$HT = $HT & "The personal license version and the commercial license version are identical without limitation. "
$HT = $HT & "Commercial licenses are available, please email me for applicable pricing." & @CRLF & @CRLF
$HT = $HT & "email: au3tools@gmail.com" & @CRLF & @CRLF
$HT = $HT & "Visit my page at http:\\au3tools.googlepages.com" & @CRLF & @CRLF
$HT = $HT & "ListIt, Copyright (c) 2007, Peter Russell"

$HT1 =       "ListIt is a tool that creates a file list as text and copies it to the clipboard." & @CRLF & @CRLF
$HT1 = $HT1 & "This is useful when a list of file names from a folder is needed to be copied "
$HT1 = $HT1 & "into another application as text." & @CRLF & @CRLF
$HT1 = $HT1 & "You may type or paste the path of the folder into the address bar, or press the "
$HT1 = $HT1 & "[...] folder browse button to navigate folders." & @CRLF & @CRLF
$HT1 = $HT1 & "You may also click on drive letters such as 'C:' or folder names such as '\data' that "
$HT1 = $HT1 & "begin with a '\' character to navigate." & @CRLF & @CRLF
$HT1 = $HT1 & "Clicking on the special entry named '..' at the top of the list takes the listing back up one folder level." & @CRLF & @CRLF
$HT1 = $HT1 & "Use the File/Explorer Path menu item to import the last path used in Windows Explorer. "
$HT1 = $HT1 & "When ListIt starts the path will default to the current Windows Explorer if one is present." & @CRLF & @CRLF
$HT1 = $HT1 & "When you have navigated to the file listing you want, click the 'Copy to Clipboard' button. "
$HT1 = $HT1 & "You may then switch to your other application and paste the folder listing text." & @CRLF

#Region ### START Koda GUI section ### Form=d:\data\my autoit\listit\listit_help.kxf
$FormHelp = GUICreate("ListIt: Help", 601, 551, 213, 121)
$HelpTab = GUICtrlCreateTab(5, 5, 591, 541)
GUICtrlSetResizing(-1, $GUI_DOCKWIDTH+$GUI_DOCKHEIGHT)
GUICtrlCreateTabItem("INTRODUCTION")
GUICtrlCreateLabel($HT, 10, 35, 582, 507)
GUICtrlCreateTabItem("FILE LISTING")
GUICtrlCreateLabel($HT1, 10, 35, 582, 507)
;GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

; Koda Notes
; Comment out GUISetState
#Region ### START Koda GUI section ### Form=D:\Data\My AutoIT\ListIt\ListIt_about.kxf
$FormAbout = GUICreate("ListIt: About", 272, 210, 193, 115)
$BLabel1 = GUICtrlCreateLabel("ListIt", 96, 8, 63, 40)
GUICtrlSetFont(-1, 22, 400, 0, "Impact")
$BLabel2 = GUICtrlCreateLabel("Copyright (c) 2007 - Peter Russell", 19, 143, 234, 20)
GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
$BLabel3 = GUICtrlCreateLabel("Written using AutoIT V3", 74, 70, 117, 17)
$BLabel4 = GUICtrlCreateLabel("http://www.autoitscript.com", 71, 85, 136, 17)
GUICtrlSetColor(-1, 0xFF0000)
$BLabel5 = GUICtrlCreateLabel("'ListIt' is free for personal use.", 62, 108, 142, 17)
$BLabel6 = GUICtrlCreateLabel("It may not be traded or sold, read the enclosed licence.", 4, 124, 263, 17)
$BLabel7 = GUICtrlCreateLabel("Version 3.0", 97, 49, 57, 17)
$BLabel8 = GUICtrlCreateLabel("email: au3tools@gmail.com", 55, 165, 132, 17)
$Label1 = GUICtrlCreateLabel("http://au3tools.googlepages.com", 38, 182, 162, 17)
;GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

;
; Start mainline initialisation
;

;text array extent size
Dim $array_ext = 100

$text_buf = ""			; text buffer to send to clipbaord
Dim $text[$array_ext]	; array to hold unsorted text
$text_i = 0				; array index (entry count)

list_files(0)

;
; Windows message handling loop
;

While 1
	$nMsg = GUIGetMsg(1)
	Switch $nMsg[0]
		Case $GUI_EVENT_CLOSE, $MenuClose
			If $nMsg[1] = $FormMain Then
				Exit
			Else
				GUISetState(@SW_HIDE, $nMsg[1])
			EndIf

		Case $Copy, $MenuCopy
			; Sort the array, generate text and put on clipboard
			$text_buf = ""
			_ArraySort($text, 0, 0, $text_i-1)
			For $i = 0 To $text_i - 1
				$text_buf = $text_buf & $text[$i] & @CRLF
			Next
			ClipPut($text_buf)

		Case $MenuPath
			$newPath = explorer_path()
			If $newpath <> "" Then
				GUICtrlSetData($Path, $newPath)
				list_files(0)
			Else
				MsgBox(0, "File List", "No Windows Explorer found.")
			EndIf

		Case $Dir, $MenuOpen
			;Pop up a dir navigation dialog tree
			$newPath = folder_dialog()
			If $newPath <> "" Then
				GUICtrlSetData($Path, $newPath)
			EndIf
			list_files(0)

		Case $Path
			list_files(0)

		Case $List
			; If the selection starts with a back-slash or the current path is empty
			If StringLeft(GUICtrlRead($List), 1) = "\" Or GUICtrlRead($Path) = "" Then
				$newPath = GUICtrlRead($Path) & GUICtrlRead($List)
				GUICtrlSetData($Path, $newPath)
				list_files(0)
			Else
				; If its the dot-dot then go up a level
				If GUICtrlRead($List) = ".." Then
					$newPath = GUICtrlRead($Path)
					$newPath = StringLeft($newPath, StringInStr($newPath, "\", 0, -1) - 1)
					GUICtrlSetData($Path, $newPath)
					if $newPath = "" Then
						; List with explorer defeat ON
						list_files(1)
					Else
						list_files(0)
					EndIf
				EndIf
			EndIf

		Case $MenuAbout
			GUISetState(@SW_SHOW, $FormAbout)

		Case $MenuSummary
			GUISetState(@SW_SHOW, $FormHelp)
	EndSwitch
WEnd

; End program
Exit
;
; function: folder_dialog
; parameters: none
; returns: string path of selected folder, removes trailing back-slash is any
; comments: uses std function to pop up folder navigation dialogue
;

Func folder_dialog()
	$s = FileSelectFolder("Choose a folder", "")
	If StringRight($s, 1) = "\" Then				; Search for a trailing back-slash
		$s = StringLeft($s, StringLen($s) - 1)		; remove
	EndIf

	Return $s
EndFunc

;
; function: explorer_path
; parameters: none
; returns: string path from most current explorer window else null string
; comments: uses std functions to test for window and get text from control
;

Func explorer_path()
	; Does an explorer window exist?
	; Get window handle via search spec based on class
	$hld = WinGetHandle("[Class:CabinetWClass]")
	; Handle is null if no window exists that matches class spec
	if $hld <> "" Then
		; Window exists - get path from the embedded edit control
		return ControlGetText("[Class:CabinetWClass]", "", "[CLASS:Edit]")
	Else
		return ""
	EndIf
EndFunc

;
; function: list_files
; parameters: 0 = use explorer path,  non 0 = do not search for an explorer
; returns: void
; comments: Keeps a list control and paste text buffer in parallel
;

Func list_files($a)
	; Clear list pane
	GUICtrlSetData($List, "")

	; Initialise the paste text array index
	$Text_i = 0

	; Try to set a default path based on an open explorer window if path is null
	; and defeat is off
	If GUICtrlRead($Path) = "" and $a = 0 Then
			$NewPath = explorer_path()
			GUICtrlSetData($Path, $NewPath)
	EndIf

	If GUICtrlRead($Path) = "" Then
		; No luck with explorer path, so populate with drive list
		$Ret = DriveGetDrive("ALL")
		For $i = 1 To $Ret[0]
			$s = StringUpper($Ret[$i])
			GUICtrlSetData($List, $s)
			$text_buf = $text_buf & $s & @CRLF
		Next
	Else
		; Set up the initial parent folder dot-dot entry
		GUICtrlSetData($List, "..")

		; Shows the filenames of all files in the current directory
		$search = FileFindFirstFile(GUICtrlRead($Path) & "\*")

		; Check if the search was successful
		If $search = -1 Then
			If @error = 1 Then
				GUICtrlSetData($List, "<Empty Folder>")
			Else
				GUICtrlSetData($List, "<Folder Error>")
			EndIf

			; An error has occured, leave list_files now
			Return
		EndIf

		;
		; Generate the list data by looping through the returned file and folder names
		;

		While 1
			; Get first/next file entry
			$file = FileFindNextFile($search)
			; Test for end of list
			If @error = 1 Then ExitLoop

			; Check if this entry is a folder/directory
			$attrib = FileGetAttrib(GUICtrlRead($Path) & "\" & $file)

			; This is a folder/directory name if a "D" appears in the attribute string
			If StringInStr($attrib, "D") > 0 Then
				; Set the lead character for folder/directories
				$dirlead = "\"
			Else
				$dirlead = ""
			EndIf

			; Add this entry to the list control
			GUICtrlSetData($List, $dirlead & $file)

			; Add this entry to the paste text array
			$text[$text_i] = $dirlead & $file
			$text_i = $text_i + 1

			If $text_i >= UBound($text) Then
				ReDim $text[UBound($text) + $array_ext]
			EndIf
		WEnd

		; Close the search handle
		FileClose($search)

	EndIf

EndFunc
