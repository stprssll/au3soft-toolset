#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <GUIListBox.au3>
#include <WindowsConstants.au3>

#Region ### START Koda GUI section ### Form=c:\users\admin\documents\steph\autoit\dropit\window_list.kxf
$AForm1 = GUICreate("DropIt", 283, 300, 201, 116, BitOR($GUI_SS_DEFAULT_GUI,$WS_SIZEBOX,$WS_THICKFRAME))
$AList1 = GUICtrlCreateList("", 0, 0, 281, 266, BitOR($GUI_SS_DEFAULT_LIST,$WS_HSCROLL))
$AButton1 = GUICtrlCreateButton("Drop", 5, 272, 65, 25, $BS_NOTIFY)
$AButton2 = GUICtrlCreateButton("Refresh", 75, 272, 65, 25, $BS_NOTIFY)
$AboutButton = GUICtrlCreateButton("?", 256, 272, 25, 25)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

#Region ### START Koda GUI section ### Form=c:\users\admin\documents\steph\autoit\dropit\about.kxf
$AForm2 = GUICreate("About", 267, 153, 493, 118)
$Label1 = GUICtrlCreateLabel("DropIt", 8, 8, 53, 28)
GUICtrlSetFont(-1, 14, 400, 0, "MS Sans Serif")
$Label2 = GUICtrlCreateLabel("DropIt displays a list of current visible windows.", 8, 50, 233, 17)
$Label3 = GUICtrlCreateLabel("Select the window and click 'Drop'.", 8, 70, 185, 17)
$Label4 = GUICtrlCreateLabel("This will send the current paste buffer to that window.", 8, 90, 257, 17)
$OKButton = GUICtrlCreateButton("OK", 184, 120, 75, 25)
$Label5 = GUICtrlCreateLabel("Version 1.0.0", 8, 128, 66, 17)
#GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###


Opt("SendKeyDelay", 10) ; milliseconds

$auto = 0
Refresh()

While 1
	$msg = GuiGetMsg()
	Select
	Case $msg = $GUI_EVENT_CLOSE
		ExitLoop
	Case $msg = $AButton1
		Paste()
	Case $msg = $AButton2
		Refresh()
	Case $msg = $AboutButton
		GUISetState(@SW_SHOW, $AForm2)
	Case $msg = $OKButton
		GUISetState(@SW_HIDE, $AForm2)
	Case Else
		;;;
	EndSelect
WEnd

Exit

Func IsVisible($handle)
  If BitAnd( WinGetState($handle), 2 ) Then
    Return 1
  Else
    Return 0
  EndIf
EndFunc

Func Refresh()
	$var = WinList()
	GUICtrlSetData($AList1, "")

	For $i = 1 to $var[0][0]
		; Only display visble windows that have a title
		If $var[$i][0] <> "" AND IsVisible($var[$i][1]) Then
			; MsgBox(0, "Details", "Title=" & $var[$i][0] & @LF & "Handle=" & $var[$i][1])
			GUICtrlSetData($AList1, $var[$i][0])
		EndIf
	Next
EndFunc

Func Paste()
	$clip = ClipGet()
	WinActivate(GUICtrlRead($AList1))
	Send ($clip)
	WinActivate("Window List")
EndFunc