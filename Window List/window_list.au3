#include <GUIConstants.au3>

#Region ### START Koda GUI section ### Form=d:\data\my autoit\window_list.kxf
$Form1 = GUICreate("Window List", 202, 298, 202, 117, BitOR($WS_MINIMIZEBOX,$WS_SIZEBOX,$WS_THICKFRAME,$WS_SYSMENU,$WS_CAPTION,$WS_POPUPWINDOW,$WS_GROUP,$WS_BORDER,$WS_CLIPSIBLINGS))
$List1 = GUICtrlCreateList("", 0, 0, 201, 266, BitOR($LBS_SORT,$LBS_STANDARD,$WS_HSCROLL,$WS_VSCROLL,$WS_BORDER))
$Button1 = GUICtrlCreateButton("Minimise All", 0, 272, 201, 25, 0)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

$var = WinList()

For $i = 1 to $var[0][0]
	; Only display visble windows that have a title
	If $var[$i][0] <> "" AND IsVisible($var[$i][1]) Then
    ; MsgBox(0, "Details", "Title=" & $var[$i][0] & @LF & "Handle=" & $var[$i][1])
		GUICtrlSetData($List1, $var[$i][0])
	EndIf
Next

While 1
	$msg = GuiGetMsg()
	Select
	Case $msg = $GUI_EVENT_CLOSE
		ExitLoop
	Case $msg = $List1
		;MsgBox (0, "Event", GUICtrlRead($List_1))
		WinActivate(GUICtrlRead($List1))
		WinActivate("Window List")
	Case $msg = $Button1
		WinMinimizeAll()
		WinActivate("Window List")
	Case Else
		;;;
	EndSelect
WEnd
Exit

Func IsVisible($handle)
  If BitAnd( WinGetState($handle), 2 ) Then 
    Return 1
  Else
    Return 0
  EndIf
EndFunc

